import React, {Component} from "react";
import {Nav} from "react-bootstrap";

class Header extends Component {
    render() {
        return (
            <div>
                <Nav className='mx-auto'>
                    <Nav.Link href='/'>User</Nav.Link>
                    <Nav.Link href='/expert'>Expert</Nav.Link>
                </Nav>
            </div>
        );
    }
}

export default Header;