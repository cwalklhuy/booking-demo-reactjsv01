import React from "react";
import './BookingTable.css';

const BookingTable = (props) => {
    const {repos} = props;
    if (!repos || repos.length === 0) return <p>No repos, Sorry</p>;

    //cmt
    function renderTableData() {
        return repos.object.map((userBooking, index) => {
            const {username, bookingId, name, startTime, endTime, dateCreated, status} = userBooking;
            return (
                <tr key={username}>
                    <td>{bookingId}</td>
                    <td>{name}</td>
                    <td>{startTime}</td>
                    <td>{endTime}</td>
                    <td>{dateCreated}</td>
                    <td>{status}</td>
                </tr>
            );
        });
    };

    return (
        <div>
            <h2 className="list-head" id='title'>This is your booking</h2>
            <table id='userbookings'>
                <thead>
                <tr>
                    <th>BookingID</th>
                    <th>Name</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Date Created</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                {renderTableData()}
                </tbody>
            </table>
        </div>
    )
}

export default BookingTable;