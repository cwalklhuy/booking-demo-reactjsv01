import React, {useEffect, useState} from "react";
import axios from "axios";
import WithListLoading from "../../authentication/WithListLoading";
import BookingTable from "../booking_table/BookingTable";

function User() {

    const TableLoading = WithListLoading(BookingTable);
    const [appState, setAppState] = useState({
        loading: false,
        repos: null
    });

    useEffect(() => {
        setAppState ({loading: true});
        const apiUrl = `http://localhost:8099/bookings/root`;
        axios.get(apiUrl).then((repos) => {
            const allRepos = repos.data;
            setAppState({loading: false, repos: allRepos});
        });
    }, [setAppState]);

    return (
      <div>
          <h2>UserComponent</h2>
          <div>
              {/*<BookingTable isLoading={appState.loading} repos={appState.repos}/>*/}
              <TableLoading isLoading={appState.loading} repos={appState.repos}/>
          </div>
      </div>
    );
}

export default User;