import React from 'react';
import './App.css';
import Header from "./component/header/Header";
import User from "./component/usercomponent/UserComponent";
import Expert from "./component/expertcomponent/ExpertComponent";
import {
  Route,
  Switch
} from 'react-router-dom';
function App() {


  return (
      <div>
        <Header/>
        <Switch>
          <Route exact path='/' component={User}/>
          <Route exact path='/expert' component={Expert}/>
        </Switch>
      </div>
  );
}

export default App;
